% Bug fixed
% Leah Rowe
% 30 March 2024

We found a bug in our build instructions: when building a matrioska we
ended up with another one inside. The bug is being investigated.
